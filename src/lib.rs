use imgui::{DrawCmd, DrawCmdParams, DrawData, DrawVert, FontAtlasTexture, TextureId, Textures};
use luminance::tess::View;
use luminance_derive::{Semantics, UniformInterface, Vertex};
use luminance_front::{
    blending::{Blending, Equation, Factor},
    context::GraphicsContext,
    pipeline::{Pipeline, TextureBinding},
    pixel::{NormRGBA8UI, NormUnsigned},
    render_state::RenderState,
    shader::{Program, Uniform},
    shading_gate::ShadingGate,
    tess::{Mode, Tess},
    texture::{Dim2, GenMipmaps, MagFilter, MinFilter, Sampler, Texture},
};
use snafu::Snafu;

#[cfg(feature = "glfw")]
use luminance_glfw::GlfwSurface as Surface;
#[cfg(feature = "glutin")]
use luminance_glutin::GlutinSurface as Surface;

pub const VERT_SHADER: &str = include_str!("shader.vert");
pub const FRAG_SHADER: &str = include_str!("shader.frag");

/// ```glsl
/// in vec2 pos;
/// in vec2 uv;
/// in vec4 col;
/// ```
#[derive(Debug, Clone, Copy, PartialEq, Eq, Semantics)]
pub enum VertexSemantics {
    #[sem(name = "pos", repr = "[f32; 2]", wrapper = "VertexPos")]
    Position,
    #[sem(name = "uv", repr = "[f32; 2]", wrapper = "VertexUV")]
    UV,
    #[sem(name = "col", repr = "[u8; 4]", wrapper = "VertexCol")]
    Color,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Vertex)]
#[vertex(sem = "VertexSemantics")]
struct Vertex {
    position: VertexPos,
    uv: VertexUV,
    #[vertex(normalized = "true")]
    color: VertexCol,
}

#[derive(UniformInterface)]
struct ShaderInterface {
    pub matrix: Uniform<[[f32; 4]; 4]>,
    #[uniform(unbound)]
    pub tex: Uniform<TextureBinding<Dim2, NormUnsigned>>,
}

impl From<&DrawVert> for Vertex {
    fn from(v: &DrawVert) -> Self {
        Self {
            position: VertexPos::new(v.pos),
            uv: VertexUV::new(v.uv),
            color: VertexCol::new(v.col),
        }
    }
}

#[derive(Debug, Snafu)]
pub enum PipelineError {
    #[snafu(display("Failed to Render imgui"))]
    RenderError,

    #[snafu(display("Framebuffer at a non-sane size, did you initialize imgui correctly?"))]
    NonSaneFrameBuffer,
}

impl From<()> for PipelineError {
    fn from(_: ()) -> Self {
        Self::RenderError
    }
}

impl From<luminance_front::pipeline::PipelineError> for PipelineError {
    fn from(_: luminance_front::pipeline::PipelineError) -> Self {
        Self::RenderError
    }
}

pub struct Renderer {
    font_texture: Texture<Dim2, NormRGBA8UI>,
    textures: Textures<Texture<Dim2, NormRGBA8UI>>,
    program: Option<Program<VertexSemantics, (), ShaderInterface>>,

    tesses: Option<Vec<Tess<Vertex, u16>>>,
}

impl Renderer {
    pub fn new(surface: &mut Surface, imgui: &mut imgui::Context) -> Self {
        let mut font_atlas = imgui.fonts();
        let font = font_atlas.build_rgba32_texture();

        let program = surface
            .new_shader_program::<VertexSemantics, (), ShaderInterface>()
            .from_strings(VERT_SHADER, None, None, FRAG_SHADER)
            .unwrap()
            .ignore_warnings();

        let textures = Textures::new();

        Self {
            font_texture: upload_font_texture(surface, font),
            textures,
            program: Some(program),
            tesses: Some(Vec::new()),
        }
    }

    fn lookup_texture(&mut self, texture_id: TextureId) -> &mut Texture<Dim2, NormRGBA8UI> {
        if let Some(texture) = self.textures.get_mut(texture_id) {
            texture
        } else {
            &mut self.font_texture
        }
    }

    pub fn prepare(&mut self, surface: &mut Surface, draw_data: &DrawData) {
        let mut tesses = self.tesses.take().unwrap();
        tesses.clear();
        for draw_list in draw_data.draw_lists() {
            let vtx_buffer = draw_list
                .vtx_buffer()
                .iter()
                .map(Vertex::from)
                .collect::<Vec<_>>();
            let indices = draw_list.idx_buffer();
            let tess = surface
                .new_tess()
                .set_indices(indices)
                .set_vertices(vtx_buffer)
                .set_mode(Mode::Triangle)
                .build()
                .unwrap();
            tesses.push(tess);
        }
        self.tesses = Some(tesses);
    }

    pub fn render(
        &mut self,
        pipeline: &Pipeline,
        shd_gate: &mut ShadingGate,
        draw_data: &DrawData,
    ) -> Result<(), PipelineError> {
        let fb_width = draw_data.display_size[0] * draw_data.framebuffer_scale[0];
        let fb_height = draw_data.display_size[1] * draw_data.framebuffer_scale[1];
        if !(fb_width >= 0.0 && fb_height >= 0.0) {
            return Err(PipelineError::NonSaneFrameBuffer);
        }

        let matrix: [[f32; 4]; 4] = {
            let left = draw_data.display_pos[0];
            let right = draw_data.display_pos[0] + draw_data.display_size[0];
            let top = draw_data.display_pos[1];
            let bottom = draw_data.display_pos[1] + draw_data.display_size[1];

            [
                [(2.0 / (right - left)), 0.0, 0.0, 0.0],
                [0.0, (2.0 / (top - bottom)), 0.0, 0.0],
                [0.0, 0.0, -1.0, 0.0],
                [
                    (right + left) / (left - right),
                    (top + bottom) / (bottom - top),
                    0.0,
                    1.0,
                ],
            ]
        };
        let clip_off = draw_data.display_pos;
        let clip_scale = draw_data.framebuffer_scale;

        let tesses = self.tesses.take().unwrap();
        let mut program = self.program.take().unwrap();

        shd_gate.shade::<(), _, _, _, _>(&mut program, |mut iface, uni, mut rdr_gate| {
            for (i, draw_list) in draw_data.draw_lists().enumerate() {
                let tess = &tesses[i];

                let mut idx_start = 0;

                // Projection and texture are common
                iface.set(&uni.matrix, matrix);

                rdr_gate.render::<(), _>(
                    &RenderState::default()
                        .set_blending(Blending {
                            equation: Equation::Additive,
                            src: Factor::SrcAlpha,
                            dst: Factor::SrcAlphaComplement,
                        })
                        .set_depth_test(None),
                    |mut tess_gate| {
                        for cmd in draw_list.commands() {
                            match cmd {
                                DrawCmd::Elements {
                                    count,
                                    cmd_params:
                                        DrawCmdParams {
                                            clip_rect,
                                            texture_id,
                                            ..
                                        },
                                } => {
                                    let idx_end = idx_start + count;
                                    let clip_rect = [
                                        (clip_rect[0] - clip_off[0]) * clip_scale[0],
                                        (clip_rect[1] - clip_off[1]) * clip_scale[1],
                                        (clip_rect[2] - clip_off[0]) * clip_scale[0],
                                        (clip_rect[3] - clip_off[1]) * clip_scale[1],
                                    ];
                                    if clip_rect[0] < fb_width
                                        && clip_rect[1] < fb_height
                                        && clip_rect[2] >= 0.0
                                        && clip_rect[3] >= 0.0
                                    {
                                        let texture = pipeline
                                            .bind_texture(self.lookup_texture(texture_id))
                                            // TODO early return instead of unwrap
                                            .unwrap();

                                        iface.set(&uni.tex, texture.binding());

                                        // TODO early return instead of unwrap
                                        tess_gate.render(tess.view(idx_start..idx_end).unwrap())?;
                                    }
                                    idx_start = idx_end;
                                }
                                _ => {}
                            }
                        }

                        Ok(())
                    },
                )?;
            }

            Ok(())
        })?;

        self.tesses = Some(tesses);
        self.program = Some(program);

        Ok(())
    }
}

fn upload_font_texture(
    surface: &mut Surface,
    font: FontAtlasTexture,
) -> Texture<Dim2, NormRGBA8UI> {
    let mut sampler = Sampler::default();
    sampler.mag_filter = MagFilter::Linear;
    sampler.min_filter = MinFilter::Linear;
    let mut tex = Texture::new(surface, [font.width, font.height], 0, sampler)
        .expect("Failed to create imgui font texture");

    tex.upload_raw(GenMipmaps::No, font.data).unwrap();

    tex
}
