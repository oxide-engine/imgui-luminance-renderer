uniform mat4 matrix;

in vec2 pos;
in vec2 uv;
in vec4 col;

out vec2 f_uv;
out vec4 f_col;

void main() {
    f_uv = uv;
    f_col = col;

    gl_Position = matrix * vec4(pos.xy, 0, 1);
}
