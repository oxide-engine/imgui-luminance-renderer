uniform sampler2D tex;

in vec2 f_uv;
in vec4 f_col;

out vec4 out_color;

void main() {
    out_color = f_col * texture(tex, f_uv.st);
}
